﻿ProductoCliente.getAll().done(function (productos) {
    var rows = '';
    console.log(JSON.stringify(productos));
    productos.forEach(function (producto) {
        rows += '<tr row_id=' + producto.ID + '>';
        rows += '<td><a class="delete" id=' + producto.ID + ' href="#">Eliminar</a></td>';
        rows += '<td>' + producto.Precio + '</td>';
        rows += '<td>' + producto.ModeloMarca + '</td>';
        rows += '<td>' + producto.Categoria + '</td>';
        rows += '<td>' + producto.Proveedor + '</td>';
        rows += '</tr>';
    });
    $('#productos').append(rows);
});
// objteto tipo Resource
var Resource = function (resource) {
    this.resource = resource;
    this.getAll = function () {
        return $.ajax({ url: '/api/' + this.resource });
    };
    this.create = function (productoDTO) {
        return $.ajax({
            type: "POST",
            url: '/api/' + this.resource,
            data: productoDTO
        });
    };
    this.delete = function (id) {
        return $.ajax({
            type: "DELETE",
            url: '/api/' + this.resource + '/' + id
        });
    };
};

//creamos uno por cada entidad de objeto creado arriba que recibe el nombre de la entidad
var ProductoCliente = new Resource('productos');

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AjaxExample.Models
{
    [Table("Productos")]
    public class Producto
    {
        public int ID { get; set; }
        public double Precio { get; set; }
        public string Marca { get; set; }
        public string Categoria { get; set; }
        public string Proveedor { get; set; }
    }
}